const gulp = require('gulp'),
  browserSync = require('browser-sync').create(),
  imagemin = require('gulp-imagemin'),
  uglify = require('gulp-uglify'),
  sass = require('gulp-sass'),
  autoprefixer = require('autoprefixer'),
  postcss = require('gulp-postcss');
  // cssnano = require('cssnano')

  function errorLog(error) {
    console.log(error);
    this.emit('end');
  }

// Static server & watch scss + html files
gulp.task('watch', ['scss'], function() {

  browserSync.init({
    server: '.'
  });

  gulp.watch('./scss/**/*.scss', ['scss'], browserSync.reload);
  gulp.watch('./*.html').on('change', browserSync.reload);
  gulp.watch('./js/**/*.js', ['scripts'], browserSync.reload);
  gulp.watch('img/*', ['image'], browserSync.reload);

});

// Scripts Task
// Uglifies,

gulp.task('scripts', function() {
  gulp.src('js/*.js')
    .pipe(uglify())
    // .pipe(plumber())Used to handle errors and stop it crashing
    .on('error', errorLog)
    .pipe(gulp.dest('build/js'));
});

// Image Task

gulp.task('image', function() {
  return gulp.src('img/*')
    .pipe(imagemin())
    .pipe(gulp.dest('img/'));
});



gulp.task('scss', function () {
    var processors = [
        autoprefixer({
          browsers:['last 10 version']
        }),
        // cssnano
    ];
    return gulp.src('./scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss(processors))
        .pipe(sass({
               outputStyle: 'expanded'
             }))
        .pipe(gulp.dest('css'))
        .pipe(browserSync.stream());
});



// default will also watch
gulp.task('default', ['watch', 'image']);
